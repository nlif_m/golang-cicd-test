FROM golang:1.20-alpine as builder


WORKDIR /app
COPY . .

RUN go fmt . && go vet . && go build .

FROM alpine:3.18 as runner

EXPOSE 8080

COPY --from=builder /app/helloweb /app/helloweb

CMD ["/app/helloweb"]




