package main

import (
	"fmt"
	"log"
	"net/http"
)

func hello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Accepted connection from", req.UserAgent(), req.RemoteAddr)
	fmt.Fprint(res, "Hello, Man!\n")
}

func main() {
	http.HandleFunc("/", hello)
	log.Fatalln(http.ListenAndServe(":8080", nil))
}
